#pragma  once
#include <opencv2/opencv.hpp>

class ImageList
{
public:
    ImageList(std::string fileName);

    cv::Mat operator[](unsigned int index);

    unsigned long size(void);

private:
    std::vector<std::string> imageFileNames;
};


