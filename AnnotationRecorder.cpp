#include <fstream>
#include <iterator>
#include "AnnotationRecorder.h"

Annotation::Annotation(std::initializer_list<int> init)
{
    auto loc = init.begin();

    center.x = *loc;

    loc++;

    center.y = *loc;

    loc++;

    radius = *loc;
}

bool Annotation::empty(void)
{
    return center == cv::Point(-1, -1);
}

bool Annotation::centerOnly(void)
{
    return center != cv::Point(-1, -1) && radius == -1;
}

bool Annotation::complete(void)
{
    return center != cv::Point(-1, -1) && radius != -1;
}

AnnotationRecorder::AnnotationRecorder(int size)
    : annotations(size)
{

}

void AnnotationRecorder::record(int index, Annotation annotation)
{
    annotations[index] = annotation;
}

Annotation &AnnotationRecorder::operator[](unsigned int index)
{
    return annotations[index];
}

void AnnotationRecorder::save(std::string filename)
{
    std::ofstream writer(filename);

    std::transform(annotations.begin(), annotations.end(), std::ostream_iterator<std::string>(writer), [](auto a)
    {
        std::ostringstream formatter;
        formatter << a.center.x << " " << a.center.y << " " << a.radius << std::endl;

        return formatter.str();
    });
}

void AnnotationRecorder::load(std::string filename)
{
    std::ifstream reader(filename);

    std::string line;
    int index = 0;
    while(std::getline(reader, line))
    {
        std::istringstream format(line);

        Annotation current;
        format >> current.center.x >> current.center.y >> current.radius;

        annotations[index++] = current;
    }
}