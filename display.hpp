#pragma once

#include <opencv2/opencv.hpp>
#include <functional>
#include "AnnotationRecorder.h"

class Display
{
friend void opencvHookMouse(int event, int x, int y, int flags, void *userdata);

public:
    Display(std::function<void(cv::Point)> moveCb, std::function<void(cv::Point)> clickCb);

    char image(cv::Mat img);

    void update(Annotation a);
    void update(double tmpRadius);


private:
    cv::Mat makeBuffer(void);
    void draw(cv::Mat m, double tmpRadius = -1);
    void display(double tmpRadius = -1);

    cv::Mat original;
    Annotation data;

    std::function<void(cv::Point)> move;
    std::function<void(cv::Point)> click;
};