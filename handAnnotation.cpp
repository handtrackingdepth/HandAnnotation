#include <opencv2/opencv.hpp>
#include "AnnotationRecorder.h"
#include "imageList.h"
#include "display.hpp"

const char ESCAPE = 27;
const char SPACE = 32;
const char BACKSPACE = 8;

Annotation current;
Display *disp;

void printUsage(void)
{
    std::cout << "Usage: handAnnotation [input image list] [output track]" << std::endl;
}

void printControls(void)
{
    std::cout << "Controls" << std::endl;
    std::cout << "========" << std::endl;
    std::cout << "escape: clear current annotation frame" << std::endl;
    std::cout << "space: advance one frame (store current annotation if complete)" << std::endl;
    std::cout << "backspace: rewind one frame (store current annotation if complete)" << std::endl;
    std::cout << "s: save all annotation (including unannotated frames)" << std::endl;
    std::cout << "r: reload stored annotation (discarding changes)" << std::endl;
    std::cout << "q: quit (does NOT save changes)" << std::endl;
    std::cout << "l: load saved annotations from output file" << std::endl;
    std::cout << "a: advance to nearest unannotated frame (does NOT store changes)" << std::endl;
}

void advance(AnnotationRecorder rec, int max, int &index)
{
    for(;index < max; index++)
    {
        Annotation cur = rec[index];

        if(cur.empty())
            return;
    }
}

void mouseClick(cv::Point pt)
{
    if(current.empty())
    {
        current.center = pt;
        disp->update(current);
    }
    else
    {
        double radius = cv::norm(current.center - pt);
        current.radius = radius;
        disp->update(current);
    }
}

void mouseMove(cv::Point pt)
{
    if(current.centerOnly())
    {
        double radius = cv::norm(current.center - pt);
        disp->update(radius);
    }
}

void writeStatusLine(int index, unsigned long max, std::string info = "")
{
    static unsigned long statusLineLength = 0;
    std::cout << '\r';

    for(int i = 0; i < statusLineLength; i++)
        std::cout << ' ';

    std::cout.flush();

    std::ostringstream formatter;
    formatter << index + 1 << '/' << max << ' ' << info;

    statusLineLength = formatter.str().size();

    std::cout << '\r' << formatter.str();
    std::cout.flush();
}

int main(int argc, char **argv)
{
    if(argc < 3)
    {
        printUsage();
        return 0;
    }

    printControls();

    std::string input(argv[1]);
    std::string output(argv[2]);

    ImageList images(input);

    AnnotationRecorder rec(static_cast<int>(images.size()));

    Display d(mouseMove, mouseClick);
    disp = &d;

    int index = 0;

    char key = '\0';
    std::string info = "";
    do
    {
        writeStatusLine(index, images.size(), info);

        cv::Mat image = images[index];
        current = rec[index];

        d.image(image);
        d.update(current);

        bool cont = false;
        do
        {
            key = static_cast<char>(cv::waitKey(0));

            switch (key)
            {
                case ESCAPE:
                    info = "discarded in-progress annotation";
                    current = Annotation();
                    d.update(current);
                    break;

                case SPACE:
                    if (current.complete())
                        rec.record(index, current);

                    info = "";

                    index = std::min(index + 1, static_cast<int>(images.size() - 1));
                    cont = true;
                    break;

                case BACKSPACE:
                    if (current.complete())
                        rec.record(index, current);

                    info = "";

                    index = std::max(index - 1, 0);
                    cont = true;
                    break;

                case 's':
                    info = "saved";
                    rec.save("gt.trk");
                    break;

                case 'r':
                    info = "reloaded stored annotation";
                    current = rec[index];
                    break;

                case 'q':
                    cont = true;
                    break;

                case 'l':
                    info = "loaded annoations from file";
                    rec.load(output);
                    cont = true;
                    break;

                case 'a':
                    info = "";
                    advance(rec, images.size(), index);
                    cont = true;
                    break;
            }

            writeStatusLine(index, images.size(), info);
        }
        while(!cont);
    }
    while(key != 'q');

    std::cout << std::endl;

    return 0;
}