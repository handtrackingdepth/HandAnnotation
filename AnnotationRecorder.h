#pragma  once

#include <opencv2/opencv.hpp>

class Annotation
{
public:
    Annotation(void) {};
    Annotation(std::initializer_list<int> init);

    cv::Point center = { -1, -1 };
    double radius = -1;

    bool empty(void);
    bool centerOnly(void);
    bool complete(void);
};

class AnnotationRecorder
{
public:
    AnnotationRecorder(int size);

    void record(int index, Annotation annotation);

    Annotation &operator[](unsigned int index);

    void save(std::string filename);
    void load(std::string filename);

private:
    std::vector<Annotation> annotations;
};


