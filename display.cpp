#include "display.hpp"
#include <opencv2/opencv.hpp>

const std::string TITLE("Annotate");

void opencvHookMouse(int event, int x, int y, int flags, void *userdata)
{
    Display *disp = reinterpret_cast<Display *>(userdata);

    if(event == cv::EVENT_LBUTTONUP)
        disp->click(cv::Point(x / 2, y / 2));
    else if(event == cv::EVENT_MOUSEMOVE)
        disp->move(cv::Point(x / 2, y / 2));
}

Display::Display(std::function<void(cv::Point)> moveCb, std::function<void(cv::Point)> clickCb)
    : move(moveCb), click(clickCb)
{
    cv::namedWindow(TITLE, cv::WINDOW_AUTOSIZE);
    cv::setMouseCallback(TITLE, opencvHookMouse, this);
}

cv::Mat Display::makeBuffer(void)
{
    cv::Mat buffer = original.clone();
    cv::resize(buffer, buffer, cv::Size(original.cols * 2, original.rows * 2));

    return buffer;
}

char Display::image(cv::Mat img)
{
    original = img;
    data = Annotation();
}

void Display::update(Annotation a)
{
    data = a;

    display();
}

void Display::update(double tmpRadius)
{
    display(tmpRadius);
}

void Display::draw(cv::Mat m, double tmpRadius)
{
    if(data.centerOnly() || data.complete())
    {
        cv::circle(m, data.center * 2, 2, cv::Scalar(0, 255, 0), 2);

        if(tmpRadius != -1)
        {
            cv::circle(m, data.center * 2, int(tmpRadius * 2), cv::Scalar(0, 0, 255));
        }
        else if(data.complete())
        {
            cv::circle(m, data.center * 2, int(data.radius * 2), cv::Scalar(0, 255, 0));
        }
    }
}

void Display::display(double tmpRadius)
{
    cv::Mat buffer = makeBuffer();

    draw(buffer, tmpRadius);
    cv::imshow(TITLE, buffer);
}