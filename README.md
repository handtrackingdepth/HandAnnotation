HandAnnotation
==============

Annotation tool that allows a user to choose the hand centroid and bounding circle
Can generate training masks from the annotated track